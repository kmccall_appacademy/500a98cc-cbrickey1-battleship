#has an underlying grid (2D array)
#:s = undamaged ship (or ship segment)
#:nil = empty space
#:x = destroyed space
class Board

  attr_accessor :grid

  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new (10) }
  end

  def empty?(position)
    col, row = position
    if @grid[col, row] == nil
      return true
    else
      return false
    end
  end

  def full?
    @grid.all? do |col|
      col.all? do |row_item|
        row_item != nil
      end
    end
  end


  #prints the board, with marks on any spaces that have been fired upon
  def display
  end

  #returns the number of valid targets (ships) remaining
  def count
    count = 0
    @grid.each do |col|
      col.each do |row_item|
        count += 1 if row_item == :s
      end
    end
    count
  end

  #randomly distributes ships across the board
  def populate_grid
  end


  def in_range?(position)
  end

end
