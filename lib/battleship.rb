#enforces rules and runs the game
#keeps a reference to the humanplayer and the board
class BattleshipGame
  attr_reader :board, :player

  #runs the game by calling play_turn until the game is over
  def play

    # until game_over?
    #   play_turn
    # end

  end

  #gets a guess from the player and makes an attack
  def play_turn
  end

  #marks the board at the position, destroying or replacing any ship that might be there
  def attack(position)
  end

  #prints information on the current stat of the game, including board state and the # of ships remaining
  def display_status
  end

end


#for bonus, probably need to add a class Ship
#- add a ComputerPlayer class that will fire at random positions on the board. Make it smart (not firing on same position twice)
#- refactor the game so there are 2 players, each with own board. Players should take turn firing at each other's fleet.
#- introduce a setup phase wehre each player can place ships on their board.
#- update your game to use different types of ships, each of a different size.

#Cannonical ship sizes
# Ship type / Dimensions
# Aircraft carrier / 5x1
# Battleship / 4x1
# Submarine / 3x1
# Destroyer or Cruiser / 3x1
# Patrol boat (or Destoyer) / 2x1
